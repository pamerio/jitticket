﻿using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using JitTicket.Services.TicketService.TicketContentService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.WindowsService
{
    /// <summary>
    /// Incorpora metodi per l'esecuzione del polling di check/invio/delete dei tickets passati al tornello
    /// </summary>
    public static class PollingPassTickets
    {
        /// <summary>
        /// Esegue il polling sui tickets passati al tornello
        /// </summary>
        /// <param name="ticketContentService"></param>
        /// <param name="eventLog1"></param>
        public async static Task Exec(ITicketContentService ticketContentService, EventLog eventLog1)
        {
            List<(decimal idVend, string nfcUID)> ticketsPass = null;

            #region 1° STEP - OTTENGO I BIGLIETTI PASSATI AL TORNELLO E DA INVIARE QUINDI AL CLOUD

            ticketsPass = (await ticketContentService.GetPassTickets()).ToList();

            #endregion


            if (ticketsPass != null && ticketsPass.Count > 0)
            {

                #region 2° STEP - INVIO I BIGLIETTI AL CLOUD

                (HttpStatusCode responseStatus, BaseResponseDTO response) sendTicketsPassToCloud = (HttpStatusCode.OK, new BaseResponseDTO());

                sendTicketsPassToCloud = await ticketContentService.BurnContentToCloud(new BurnTicketToCloudListDTO(ticketsPass));

                #endregion

                if (sendTicketsPassToCloud.response.ErrorCodes != Services.Common.APIErrorCodes.None || sendTicketsPassToCloud.responseStatus != HttpStatusCode.OK)
                {
                    throw new Exception($"Si è verificato un errore al momento della chiamata dell'API sendTicketsPassToCloud. HttpStatusCode: {sendTicketsPassToCloud.responseStatus} ErrorCode: {sendTicketsPassToCloud.response.ErrorCodes.ToString()}");
                }
                else
                {
                    #region 4° STEP - CANCELLO NEl DB I BIGLIETTI INVIATI AL CLOUD

                    await ticketContentService.DeletePassTicketsOnLocal(ticketsPass.Select(c => c.idVend));

                    #endregion
                }
            }

        }
    }
}
