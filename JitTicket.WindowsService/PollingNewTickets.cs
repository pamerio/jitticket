﻿using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using JitTicket.Services.TicketService.TicketContentService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.WindowsService
{
    /// <summary>
    /// Incorpora metodi per l'esecuzione del polling di check/invio/delete di nuovi tickets
    /// </summary>
    public static class PollingNewTickets
    {
        /// <summary>
        /// Esegue il polling sui nuovi tickets
        /// </summary>
        /// <param name="ticketContentService"></param>
        /// <param name="eventLog1"></param>
        public async static Task Exec(ITicketContentService ticketContentService, EventLog eventLog1)
        {
            int numTicketsNew = 0;

            #region 1° STEP - VERIFICO SE CI SONO NUOVI BIGLIETTI DA INVIARE AL CLOUD

            numTicketsNew = await ticketContentService.CheckNewTickets();

            #endregion


            if (numTicketsNew > 0)
            {
                #region 2° STEP - PRELEVO IL CONTENUTO DEI BIGLIETTI DA INVIARE AL CLOUD

                AddContentToCloudListDTO contentTickets = null;

                contentTickets = await ticketContentService.GetNewTicketsMovieContent();

                #endregion

                #region 3° STEP - INVIO IL CONTENUTO DEI BIGLIETTI AL CLOUD

                (HttpStatusCode responseStatus, BaseResponseDTO response) sendTicketsNewToCloud = (HttpStatusCode.OK, new BaseResponseDTO());

                sendTicketsNewToCloud = await ticketContentService.AddContentToCloud(contentTickets);

                #endregion

                if (sendTicketsNewToCloud.response.ErrorCodes != Services.Common.APIErrorCodes.None || sendTicketsNewToCloud.responseStatus != HttpStatusCode.OK)
                {
                    throw new Exception($"Si è verificato un errore al momento della chiamata dell'API sendTicketsNewToCloud. HttpStatusCode: {sendTicketsNewToCloud.responseStatus} ErrorCode: {sendTicketsNewToCloud.response.ErrorCodes.ToString()}");
                }
                else
                {
                    #region 4° STEP - CANCELLO NEl DB I BIGLIETTI INVIATI AL CLOUD

                    await ticketContentService.DeleteNewTicketsOnLocal(contentTickets.Select(c => c.IdVendita));

                    #endregion
                }
            }

        }
    }
}
