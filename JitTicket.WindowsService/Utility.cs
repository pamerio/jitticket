﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.WindowsService
{
    public static class Utility
    {
        public static void SendEmailErrors(Exception ex)
        {
            var toemail = ConfigurationManager.AppSettings["EMAIL_ERROR_ADDRESSES"].Split(';').ToList();
            SendEmail(toemail, BuildErrorMessageFromException(ex), "Errore servizio JitTicket");
        }
        public static void SendEmail(List<string> ToEmail, string Message, String Subj, List<string> cc = null, List<string> bcc = null)
        {
            string HostAdd = "smtp.prova.com";
            string FromEmailid = "tech@prova.it";
            string Pass = "0000";
            int port = 575;

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(FromEmailid);
            mailMessage.Subject = Subj;
            mailMessage.Body = Message;
            mailMessage.IsBodyHtml = true;

            foreach (string ToEMailId in ToEmail)
            {
                mailMessage.To.Add(new MailAddress(ToEMailId));
            }

            foreach (string CCEmail in cc)
            {
                mailMessage.CC.Add(new MailAddress(CCEmail));
            }

            foreach (string bccEmailId in bcc)
            {
                mailMessage.Bcc.Add(new MailAddress(bccEmailId));
            }
            SmtpClient smtp = new SmtpClient();
            smtp.Host = HostAdd;

            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = Pass;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = port;
            smtp.Send(mailMessage);
        }
        /// <summary>
        /// Costruisce il messaggio di errore da inviare per email comprensivo di InnerException e StackTrace sino al terzo livello
        /// </summary>
        /// <param name="ex">Eccezione da cui costruire il messaggio di errore</param>
        /// <returns></returns>
        public static string BuildErrorMessageFromException(Exception ex)
        {
            string mainExceptionMessage = ex.Message;
            string mainExceptionStackTrace = ex.StackTrace;
            string firstChildExceptionMessage = string.Empty;
            string FirstChildExceptionStackTrace = string.Empty;
            string secondChildExceptionMessage = string.Empty;
            string secondChildExceptionStackTrace = string.Empty;
            string thirdChildExceptionMessage = string.Empty;
            string thirdChildxceptionStackTrace = string.Empty;

            if (ex.InnerException != null)
            {
                firstChildExceptionMessage = ex.InnerException.Message;
                FirstChildExceptionStackTrace = ex.InnerException.StackTrace;
                var ex1 = ex.InnerException;
                if (ex1.InnerException != null)
                {
                    secondChildExceptionMessage = ex1.InnerException.Message;
                    secondChildExceptionStackTrace = ex1.InnerException.StackTrace;
                    var ex2 = ex1.InnerException;
                    if (ex2.InnerException != null)
                    {
                        thirdChildExceptionMessage = ex2.InnerException.Message;
                        thirdChildxceptionStackTrace = ex2.InnerException.StackTrace;
                    }
                }
            }
            var message = string.Format("MainException Message: {0}  -- FirstChildException Message: {1}  -- SecondChildException Message: {2}  -- ThirdChildException Message: {3}", mainExceptionMessage, firstChildExceptionMessage, secondChildExceptionMessage, thirdChildExceptionMessage);
            var stackTrace = string.Format("MainStackTrace Message: {0}  -- FirstChildExceptionStackTrace Message: {1}  -- SecondChildExceptionStackTrace Message: {2}  -- ThirdChildExceptionStackTrace Message: {3}", mainExceptionStackTrace, FirstChildExceptionStackTrace, secondChildExceptionStackTrace, thirdChildxceptionStackTrace);

            return $"<p><strong>DATA:{DateTime.Now.ToString()}</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>LOCALE:</strong> {GetLocaleName()}</p><p><strong>ERRORE:</strong> {message}</p><p><strong>STACK TRACE:</strong> {stackTrace}</p>";
        }
        public static string GetLocaleName()
        {
            try
            {
               return ConfigurationManager.AppSettings["NOME_LOCALE"];
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
