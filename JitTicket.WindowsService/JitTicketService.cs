﻿using JitTicket.Services.TicketService.TicketContentService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace JitTicket.WindowsService
{
    public partial class JitTicketService : ServiceBase
    {
        private readonly ITicketContentService _ticketContentService;
        Timer timerPollingNew = new Timer();
        Timer timerPollingPass = new Timer();


        public JitTicketService()
        {
            InitializeComponent();
            _ticketContentService = new TicketContentService();

            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("JitTicketService","."))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "JitTicketService", "Application");
            }
            eventLog1.Source = "JitTicketService";
            eventLog1.Log = "Application";
        }
        /// <summary>
        /// Metodo stub usato per debuggare il servizio lanciando OnStart dal Program.cs
        /// </summary>
        public void onStart()
        {
            try
            {
                timerPollingNew.Interval = double.Parse(ConfigurationManager.AppSettings["INTERVAL_POLLING_NEW_TICKETS"].ToString());
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Il servizio JitTicketService non può partire perchè c'è un errore nella conversione del parametro di configurazione INTERVAL_POLLING_NEW_TICKETS. Verificare che sia presente e che sia valorizzato come numero");
                throw;
            }
            timerPollingNew.Elapsed += TimerPollingNew_Elapsed;
            timerPollingNew.Start();

            try
            {
                timerPollingPass.Interval = double.Parse(ConfigurationManager.AppSettings["INTERVAL_POLLING_PASS_TICKETS"].ToString());
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Il servizio JitTicketService non può partire perchè c'è un errore nella conversione del parametro di configurazione INTERVAL_POLLING_PASS_TICKETS. Verificare che sia presente e che sia valorizzato come numero");
                throw;
            }
            timerPollingPass.Elapsed += TimerPollingPass_Elapsed;
            timerPollingPass.Start();

        }

        private async void TimerPollingPass_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Stoppo il servizio in quanto l'invio al Cloud potrebbe durare più dell'intervallo di polling
            timerPollingPass.Stop();

            try
            {
                await PollingPassTickets.Exec(_ticketContentService, eventLog1);
            }
            catch(Exception ex)
            {
                eventLog1.WriteEntry($"Si è verificato un erorre durante il PollingPassTickets. Errrore: {ex.Message}", EventLogEntryType.Error);
                Utility.SendEmailErrors(ex);
            }

            timerPollingPass.Start();
        }

        protected override void OnStart(string[] args)
        {
            onStart();
        }

        private async void TimerPollingNew_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Stoppo il servizio in quanto l'invio al Cloud potrebbe durare più dell'intervallo di polling
            timerPollingNew.Stop();

            try
            {
                await PollingNewTickets.Exec(_ticketContentService, eventLog1);
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry($"Si è verificato un erorre durante il PollingNewTickets. Errrore: {ex.Message}", EventLogEntryType.Error);
                Utility.SendEmailErrors(ex);
            }

            timerPollingNew.Start();


        }

        protected override void OnStop()
        {
        }
    }
}
