﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Dal.Domain.TicketContent
{
    /// <summary>
    /// Con previsione futura che un domani possano essere inviati al cloud Content di tipologia diversi (quindi non solo biglietti del cinema) si definisce questa interfaccia per tutti i Content
    /// </summary>
    public interface ITicketContent
    {

    }
}
