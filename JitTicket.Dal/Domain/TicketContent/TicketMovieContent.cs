﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Dal.Domain.TicketContent
{
    /// <summary>
    /// Content dei ticket del cinema
    /// </summary>
    public class TicketMovieContent : ITicketContent
    {
        public TicketMovieContent() { }
        public TicketMovieContent(DataRow row)
        {
            FromDBToEntity(row);
        }

        #region Properties

        /// <summary>
        /// Tag del NFC eventualmente collegato a questo biglietto
        /// </summary>
        [RowIndex(41)]
        [ExportableCloud("NFC_UID")]
        public string nfcUID { get; set; }
        [RowIndex(0)]
        [ExportableCloud("ID_VENDITA")]
        public decimal IdVendita { get; set; }
        /// <summary>
        /// Codice SIAE del sistema
        /// </summary>
        [RowIndex(1)]
        [ExportableCloud("COD_SISTEM")]
        public string CodSistema { get; set; }
        /// <summary>
        /// Serial Number della Smart Card
        /// </summary>
        [RowIndex(2)]
        [ExportableCloud("SERIAL_NUMBER")]
        public string SerialNumber { get; set; }
        /// <summary>
        /// Sigillo HEX della Smart Card
        /// </summary>
        [RowIndex(3)]
        [ExportableCloud("SIGILLO")]
        public string Sigillo { get; set; }
        /// <summary>
        /// Numero progressivo della Smart Card
        /// </summary>
        [RowIndex(4)]
        [ExportableCloud("NUM_PROG")]
        public decimal NumProg { get; set; }
        [RowIndex(5)]
        [ExportableCloud("DATA_EMISSIONE")]
        public string DataEmissione { get; set; }
        [RowIndex(6)]
        [ExportableCloud("ORA_EMISSIONE")]
        public string OraEmissione { get; set; }
        /// <summary>
        /// Codice Fiscale/Partita IVA dell'organizzatore
        /// </summary>
        [RowIndex(7)]
        [ExportableCloud("CF")]
        public string Cf { get; set; }
        [RowIndex(8)]
        [ExportableCloud("DATA_EVENTO")]
        public string DataEvento { get; set; }
        [RowIndex(9)]
        [ExportableCloud("ORA_EVENTO")]
        public string OraEvento { get; set; }
        /// <summary>
        /// Codice Tipo Evento
        /// </summary>
        [RowIndex(10)]
        [ExportableCloud("TE")]
        public string Te { get; set; }
        /// <summary>
        /// Descrizione tipo evento
        /// </summary>
        [RowIndex(11)]
        [ExportableCloud("EVENTO")]
        public string Evento { get; set; }
        /// <summary>
        /// S = Spettacolo I=Intrattenimento
        /// </summary>
        [RowIndex(12)]
        [ExportableCloud("SI")]
        public string Si { get; set; }
        /// <summary>
        /// Organizzatore evento
        /// </summary>
        [RowIndex(13)]
        [ExportableCloud("ORGANIZZATORE")]
        public string Organizzatore { get; set; }
        /// <summary>
        /// Titolo evento
        /// </summary>
        [RowIndex(14)]
        [ExportableCloud("TITOLO")]
        public string Titolo { get; set; }
        /// <summary>
        /// Codice SIAE sala
        /// </summary>
        [RowIndex(15)]
        [ExportableCloud("COD_LOCALE")]
        public string CodLocale { get; set; }
        /// <summary>
        /// Nome della sala
        /// </summary>
        [RowIndex(16)]
        [ExportableCloud("SALA_DESC")]
        public string SalaDesc { get; set; }
        /// <summary>
        /// Numero della sala
        /// </summary>
        [RowIndex(17)]
        [ExportableCloud("SALA_NUM")]
        public string SalaNum { get; set; }
        /// <summary>
        /// Comune del locale
        /// </summary>
        [RowIndex(18)]
        [ExportableCloud("COMUNE")]
        public string Comune { get; set; }
        [RowIndex(19)]
        [ExportableCloud("NOME_LOCALE")]
        public string NomeLocale { get; set; }
        /// <summary>
        /// Ragione Sociale titolare
        /// </summary>
        [RowIndex(20)]
        [ExportableCloud("RAG_SOC_TITOLARE")]
        public string RsTitolare { get; set; }
        /// <summary>
        /// Ragione Sociale Organizzatore
        /// </summary>
        [RowIndex(21)]
        [ExportableCloud("RAG_SOC_ORGANIZZATORE")]
        public string RsOrganizzatore { get; set; }
        /// <summary>
        /// Ordine di posto
        /// </summary>
        [RowIndex(22)]
        [ExportableCloud("ODP")]
        public string Odp { get; set; }
        /// <summary>
        /// Codice del posto
        /// </summary>
        [RowIndex(23)]
        [ExportableCloud("ID_POSTO")]
        public string IdPosto { get; set; }
        /// <summary>
        /// Identificativo fila
        /// </summary>
        [RowIndex(24)]
        [ExportableCloud("FILA")]
        public string Fila { get; set; }
        /// <summary>
        /// Identificativo posto
        /// </summary>
        [RowIndex(25)]
        [ExportableCloud("POSTO")]
        public string Posto { get; set; }
        /// <summary>
        /// Descrizione opzionale del posto
        /// </summary>
        [RowIndex(26)]
        [ExportableCloud("DESC_ESTESA")]
        public string DescEstesa { get; set; }
        /// <summary>
        /// Settore del posto
        /// </summary>
        [RowIndex(27)]
        [ExportableCloud("SETTORE")]
        public string Settore { get; set; }
        /// <summary>
        /// Campo generico che può contenere informazioni (codici) relativi all'acquisto
        /// </summary>
        [RowIndex(28)]
        [ExportableCloud("NOME_PRE")]
        public string NomePre { get; set; }
        /// <summary>
        /// Codice SIAE tipo biglietto
        /// </summary>
        [RowIndex(29)]
        [ExportableCloud("TIPO_TITOLO")]
        public string TipoTitolo { get; set; }
        /// <summary>
        /// Descrizione del biglietto
        /// </summary>
        [RowIndex(30)]
        [ExportableCloud("BIGLIETTO")]
        public string Biglietto { get; set; }
        [RowIndex(31)]
        [ExportableCloud("PREZZO_LORDO")]
        public decimal? PrezzoLordo { get; set; }
        [RowIndex(32)]
        [ExportableCloud("SUPPLEMENTO")]
        public decimal? Supplemento { get; set; }
        /// <summary>
        /// V=venduto A=annullato
        /// </summary>
        [RowIndex(33)]
        [ExportableCloud("VA")]
        public string Va { get; set; }
        /// <summary>
        /// Id dell'ordine
        /// </summary>
        [RowIndex(34)]
        [ExportableCloud("ID_ORDINE")]
        public string IdOrdine { get; set; }
        /// <summary>
        /// Id della transazione
        /// </summary>
        [RowIndex(35)]
        [ExportableCloud("ID_TRANS")]
        public string IdTrans { get; set; }
        /// <summary>
        /// Indica se è Biglietto normale, Abbonamento o Ticketo
        /// </summary>
        [RowIndex(36)]
        [ExportableCloud("TIPOLOGIA_BIGLIETTO")]
        public string TipologiaBiglietto { get; set; }
        /// <summary>
        /// Codice a barre del Ticketo
        /// </summary>
        [RowIndex(37)]
        [ExportableCloud("BARCODE_TICKETO")]
        public string BarcodeTicketo { get; set; }
        [RowIndex(38)]
        [ExportableCloud("PAGA_CLIENTE")]
        public string PagaCliente { get; set; }
        /// <summary>
        /// Id della tessera dell'abbonamento
        /// </summary>
        [RowIndex(39)]
        [ExportableCloud("ID_TESSERA")]
        public string IdTessera { get; set; }
        /// <summary>
        /// Codice dello'abbonamento
        /// </summary>
        [RowIndex(40)]
        [ExportableCloud("COD_ABBONAMENTO")]
        public string CodAbbonamento { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Mappa i campi della DataRow passata alle proprietà della classe
        /// </summary>
        /// <param name="row"></param>
        public void FromDBToEntity(DataRow row)
        {
            var objtype = this.GetType();

            foreach (PropertyInfo p in objtype.GetProperties())
            {
                // for every property loop through all attributes
                foreach (Attribute a in p.GetCustomAttributes(false))
                {
                    if(a.GetType() == typeof(RowIndexAttribute))
                    {
                        try
                        {
                            var value = row.ItemArray[((RowIndexAttribute)a).RowIndex];
                            object valueNewObject = null;

                            if(value.GetType() == typeof(System.DBNull))
                            {
                                if (p.PropertyType == typeof(string))
                                    valueNewObject = string.Empty;
                                else if (p.PropertyType == typeof(decimal?))
                                    valueNewObject = null;
                            }
                            else
                            {
                                valueNewObject = value;
                            }
                            p.SetValue(this, valueNewObject);
                        }
                        catch(Exception ex)
                        {
                            throw new Exception($"Si è verificato un problema nella fase di mappatura dei dati della query che restrituisce il contenuto dei biglietti. Verificare che non siano cambiati la struttura dei dati nel DB rispetto alla query. Errore restituito: {ex.Message}");
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Inserisce in un dizionario tutte le proprietà da esportare sul Cloud (Nome/Valore)
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,string> FromEntityToFormValues()
        {
            var returnObj = new Dictionary<string, string>();

            var objtype = this.GetType();

            foreach (PropertyInfo p in objtype.GetProperties())
            {
                // for every property loop through all attributes
                foreach (Attribute a in p.GetCustomAttributes(false))
                {
                    if (a.GetType() == typeof(ExportableCloudAttribute))
                    {
                        var exportName = ((ExportableCloudAttribute)a).Name;
                        returnObj.Add(exportName, p.GetValue(this).ToString());
                    }
                }
            }
            return returnObj;
        }
        /// <summary>
        /// Serializza come stringa Json tutte le coppie Nome/Valore delle proprietà da esportare sul Cloud
        /// </summary>
        /// <returns></returns>
        public string SerializeToJson()
        {
            return JsonConvert.SerializeObject(FromEntityToFormValues());
        }

        #endregion
    }
}
