﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Dal
{
    /// <summary>
    /// Utilizzato per definire di una data proprietà quale sia l'indice nella Row di una query select dal DB
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RowIndexAttribute : Attribute
    {
        public RowIndexAttribute(int index)
        {
            RowIndex = index;
        }
        public int RowIndex { get; set; }
    }
}
