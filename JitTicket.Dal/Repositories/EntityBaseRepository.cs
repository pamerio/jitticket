﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintic.Data.oracle;

namespace JitTicket.Dal.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T: class, new()
    {
        protected CConnectionOracle _oracleConnection;

        public CConnectionOracle OracleConnection
        {
            get
            {
                if(_oracleConnection == null)
                {
                    _oracleConnection = new CConnectionOracle();
                    _oracleConnection.ConnectionString = ConfigurationManager.AppSettings["ORACLE_CONN_STRING"];  //  "user=cinema;password=cinema;data source=cinema;";
                }
                return _oracleConnection;
            }
        }
    }
}
