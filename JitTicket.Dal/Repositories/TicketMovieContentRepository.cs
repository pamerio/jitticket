﻿using JitTicket.Dal.Domain.TicketContent;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintic.Data.oracle;
using System.Data;
using Wintic.Data;

namespace JitTicket.Dal.Repositories
{
    public class TicketMovieContentRepository : EntityBaseRepository<TicketMovieContent>, ITicketMovieContentRepository
    {
        #region Utilities


        #endregion

        public async Task<int> CheckNewTickets()
        {
            try
            {
                OracleConnection.Open();


                var dtAsync = await Task.Run(() =>
                {
                    return OracleConnection.ExecuteQuery("select count(*)  from VENDITE_JITNEW");
                });

                return Convert.ToInt32(dtAsync.Rows[0].ItemArray[0]);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                OracleConnection.Close();
            }
        }
        public async Task DeleteNewTickets(IEnumerable<decimal> IdsVendToDelete)
        {
            try
            {
                await DeleteListInTable("VENDITE_JITNEW", IdsVendToDelete);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                OracleConnection.Close();
            }

        }
        public async Task<IEnumerable<TicketMovieContent>> GetNewTicketsMovieContent()
        {
            #region ORACLE QUERY STRING - SELECT TICKET CONTENT

            /************************************************************************************************************************
             * 
             *   A T T E N Z I O N E:  La modifica di questa query può comportare malfunzionamenti all'intero servizio (anche il semplice cambio dell'ordine dei campi 
             *   Innanzi tutto verificare il Mapping nella classe TicketMovieContent ed in funzione delle modifiche vedere
             *   se è necessario apportare cambiamenti anche alla chiamata dell'Api Cloud (eventualmente modificando anche il codice dalla parte Cloud)
             *   
             ************************************************************************************************************************/

            StringBuilder sb = new StringBuilder();
            sb
                .Append("select")
                .Append(" v.idvend,")
                .Append(" nvl(sv.codice_sistema, '') as codice_sistema,")
                .Append(" nvl(sv.serial_number, '') as serial_number,")
                .Append(" nvl(sv.sigillo, '') as sigillo,")
                .Append(" nvl(sv.num_prog, '') as num_prog,")
                .Append(" to_char(nvl(sv.data_emi, sysdate), 'dd/mm/yyyy') as data_emissione,")
                .Append(" to_char(nvl(sv.data_emi, sysdate), 'HH24.mi') as ora_emissione,")
                .Append(" nvl(sv.cf, '') as cf,")
                .Append(" to_char(c.data_ora, 'yyyymmdd') as data_evento,")
                .Append(" to_char(c.data_ora, 'hh24mi') as ora_evento,")
                .Append(" c.te,")
                .Append(" te.descrizione as evento,")
                .Append(" c.si,")
                .Append(" c.organizzatore,")
                .Append(" p.descrizione as titolo,")
                .Append(" s.codice_locale,")
                .Append(" s.alias as sala,")
                .Append(" 'Sala '||to_number(s.descr) as sala,")

                .Append(" df.comune as comune,")
                .Append(" df.rag_soc as nome_locale,")
                .Append(" df.gestore as rs_titolare,")

                .Append(" NVL((SELECT PROPERTY_VALUE AS RAG_SOC")
                .Append("     FROM PARAMETRI_CASSA")
                .Append("    WHERE PROPERTY_NAME = 'FISCALE_DENORG_DEFAULT'")
                .Append("          AND EXISTS")
                .Append("                 (SELECT 1")
                .Append("                    FROM PARAMETRI_CASSA")
                .Append("                   WHERE PROPERTY_VALUE = C.ORGANIZZATORE)),")
                .Append(" (SELECT RAG_SOC")
                .Append("    FROM ORGANIZZATORI")
                .Append("   WHERE ORGANIZZATORE = C.ORGANIZZATORE)) as rs_organizzatore,")

                .Append(" p.odp as odp,")
                .Append(" v.idposto as idposto,")
                .Append(" substr(v.idposto, 1, instr(v.idposto, '/')-1) as fila,")
                .Append(" substr(v.idposto, instr(v.idposto, '/')+1) as posto,")
                .Append(" p.desc_estesa as desc_estesa,")

                .Append(" op.descrizione as settore,")
                .Append(" v.nomepre as nomepre,")
                .Append(" tb.tipo_titolo,")
                .Append(" tb.descr as biglietto,")
                .Append(" b.prezzo_lordo*100 as prezzo_lordo,")
                .Append(" decode(v.tipo, 'V', dir_prev*100, 'K', dir_internet*100, 'I', dir_internet*100, 'P', decode(dir_pren, 0, dir_prev*100, dir_pren*100), dir_prev*100) as supplemento,")

                // agg. per Ticket One
                .Append(" sv.tipo as va,")
                .Append(" nvl(cc.idordine, '') as idordine,")
                .Append(" nvl(cc.idtrans, '') as idtrans,")

                // agg. per TickeTo

                .Append(" decode(substr(tb.tasto, 1, 1), 'F', 'BIGLIETTO', 'T', 'CINECARD', 'P', 'TICKETO', 'BIGLIETTO') as tipologia_biglietto,")

                .Append(" nvl(rat.barcode, '') as barcode_ticketo,")
                .Append(" nvl(rat.pagailcliente, '1') as pagailcliente,")

                .Append(" nvl(rat.idtessera, '') as idtessera,")
                .Append(" nvl(rat.codice_abbonamento, '') as codice_abbonamento,")
                .Append(" vendite_jitnew.codice_uid")

                .Append(" from v_vendite v, v_smart_vendite sv, calendario c, pacchetto p, biglietto b, tipo_biglietto tb, sale s, (select * from posti union all select * from posti_h) p, ordinidiposto op, tipoevento te, transaction t,")
                .Append(" cc_payments cc,")
                .Append(" (select rag_soc, gestore, comune from datifissi) df,")
                .Append(" richieste_addebito_ticketo rat, vendite_jitnew")
                .Append(" where")

                        //.Append(" t.idtransaction = :idtransaction")
                        .Append(" t.idvend = vendite_jitnew.idvend")


                        .Append(" and")
                .Append(" t.idvend = v.idvend")
                .Append(" and")
                .Append(" v.tipo <> 'A'")

                .Append(" and")
                .Append(" v.idvend = cc.idvend(+)")

                .Append(" and")
                .Append(" v.idvend = rat.idvend(+)")

                .Append(" and")
                .Append(" v.idvend = sv.idvend(+)")
                .Append(" and")
                .Append(" v.idbiglietto = b.idbiglietto")
                .Append(" and")
                .Append(" b.idtipobigl = tb.idtipobigl")
                .Append(" and")
                .Append(" v.idcalend = c.idcalend")
                .Append(" and")
                .Append(" c.idpacchetto = p.idpacchetto")
                .Append(" and")
                .Append(" p.progr = 1")
                .Append(" and")
                .Append(" c.te = te.codice")
                .Append(" and")
                .Append(" c.idsala = s.idsala")
                .Append(" and")
                .Append(" s.idsala = p.idsala")
                .Append(" and")
                .Append(" v.idposto = p.idposto")
                .Append(" and")
                .Append(" p.odp = op.codice");


            // Importazione del recordset nell'ArrayList
            String oracleQueryString = sb.ToString();


            #endregion

            try
            {
                OracleConnection.Open();

                var dtAsync = await Task.Run(() =>
                {
                    return OracleConnection.ExecuteQuery(oracleQueryString);
                });

                //TODO Chiedere a Francesco come mai alcuni record presenti in vendite_jitnew non vengono restituiti dalla query!!!!!

                if (dtAsync.Rows.Count > 0)
                {
                    return dtAsync.AsEnumerable().Select(r => new TicketMovieContent(r));
                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                OracleConnection.Close();
            }

        }
        public async Task<IEnumerable<(decimal idVend, string ncfUID)>> GetPassTickets()
        {
            try
            {
                OracleConnection.Open();


                var dtAsync = await Task.Run(() =>
                {
                    return OracleConnection.ExecuteQuery("select IDVEND, CODICE_UID from VENDITE_JITPASS");
                });

                if (dtAsync.Rows.Count > 0)
                {
                    return dtAsync.AsEnumerable().Select(r => (Convert.ToDecimal(r.ItemArray[0]), r.ItemArray[1].ToString()));
                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                OracleConnection.Close();
            }
        }
        public async Task DeletePassTickets(IEnumerable<decimal> IdsVendToDelete)
        {
            try
            {
                await DeleteListInTable("VENDITE_JITPASS", IdsVendToDelete);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                OracleConnection.Close();
            }

        }
        /// <summary>
        /// Cancella nella tabella i record passati
        /// </summary>
        /// <param name="tableName">Nome della tabella in cui cancellare i record</param>
        /// <param name="IdsVendToDelete">Id dei record da cancellare</param>
        /// <returns></returns>
        private async Task DeleteListInTable(string tableName, IEnumerable<decimal> IdsVendToDelete)
        {
            OracleConnection.Open();

            StringBuilder sb = new StringBuilder();
            sb.Append($"DELETE FROM {tableName} WHERE IDVEND IN (");

            var numRecords = IdsVendToDelete.Count();

            for (int i = 0; i < numRecords; i++)
            {
                sb.Append(IdsVendToDelete.ElementAt(i));
                if (i < numRecords - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append(")");

            var dtAsync = await Task.Run(() =>
            {
                return OracleConnection.ExecuteNonQuery(sb.ToString());
            });
        }
    }
}
