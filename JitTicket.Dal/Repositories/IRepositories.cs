﻿using JitTicket.Dal.Domain.TicketContent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Dal.Repositories
{
    public interface ITicketMovieContentRepository : IEntityBaseRepository<TicketMovieContent>
    {
        /// <summary>
        /// Verifica se nella tabella VENDITE_JITNEW sono presenti dei record e quindi nuovi biglietti
        /// </summary>
        /// <returns>
        /// Numero dei record presenti nella tabella VENDITE_JITNEW
        /// </returns>
        Task<int> CheckNewTickets();
        /// <summary>
        /// Elimina i tickets passati dalla tabella VENDITE_JITNEW
        /// </summary>
        /// <returns></returns>
        Task DeleteNewTickets(IEnumerable<decimal> IdsVendToDelete);
        /// <summary>
        /// Restituisce il contenuto dei nuovi biglietti Movie il cui IdVend è presente nella tabella VENDITE_JITNEW
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TicketMovieContent>> GetNewTicketsMovieContent();
        /// <summary>
        /// Verifica se nella tabella VENDITE_JITPASS sono presenti dei record e quindi nuovi biglietti
        /// </summary>
        /// <returns>
        /// Numero dei record presenti nella tabella VENDITE_JITPASS
        /// </returns>
        Task<IEnumerable<(decimal idVend, string ncfUID)>> GetPassTickets();
        /// <summary>
        /// Elimina i tickets passati dalla tabella VENDITE_JITPASS
        /// </summary>
        /// <returns></returns>
        Task DeletePassTickets(IEnumerable<decimal> IdsVendToDelete);

    }
}
