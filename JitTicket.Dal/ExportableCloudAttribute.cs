﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Dal
{
    /// <summary>
    /// Utilizzato per definire di una data proprietà debba essere inviata al Cloud e quale sia il nome con cui verrà inviata
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ExportableCloudAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Nome con cui verrà inviata al Cloud questa proprietà</param>
        public ExportableCloudAttribute(string name)
        {
            Name = name;
        }
        /// <summary>
        /// Nome con cui verrà inviata al Cloud questa proprietà
        /// </summary>
        public string Name { get; set; }
    }
}
