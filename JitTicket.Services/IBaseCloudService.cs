﻿using JitTicket.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services
{
    public interface IBaseCloudService
    {
        /// <summary>
        /// Esegue una chiamata POST al dato endpoint con content type = application/json
        /// </summary>
        /// <typeparam name="Tdata">Tipo dei dati da inviare</typeparam>
        /// <typeparam name="TResult">Tipo dei dati restituiti dalla chiamata</typeparam>
        /// <param name="endPoint">Indirizzo endpoint</param>
        /// <param name="data">Dati da inviare</param>
        /// <returns></returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostAsync<Tdata>(string endPoint, Tdata data) where Tdata : ISerializableContent;
        /// <summary>
        /// Esegue una chiamata POST al dato endpoint
        /// </summary>
        /// <typeparam name="TResult">Tipo dei dati restituiti dalla chiamata</typeparam>
        /// <param name="endPoint">Indirizzo endpoint</param>
        /// <param name="jsonContent">Contenuto da inviare serializzato in Json</param>
        /// <returns></returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostAsync(string endPoint, string jsonContent);
        /// <summary>
        /// Esegue una chiamata POST al dato endpoint con content type = application/x-www-form-urlencoded
        /// </summary>
        /// <typeparam name="Tdata">Tipo dei dati da inviare</typeparam>
        /// <typeparam name="TResult">Tipo dei dati restituiti dalla chiamata</typeparam>
        /// <param name="endPoint">Indirizzo endpoint</param>
        /// <param name="data">Dati da inviare</param>
        /// <returns></returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostFormAsync<Tdata>(string endPoint, Tdata data) where Tdata : IFormableContent;
        /// <summary>
        /// Esegue una chiamata GET al dato endpoint
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> GetAsync<TResult>(string endPoint);
    }

    /// <summary>
    /// Interfaccia utilizzata per gli oggetti da passare alle chiamate Post e che utilizzano un content type = application/json. Tali oggetti devono essere Json serializzabili
    /// </summary>
    public interface ISerializableContent
    {
        string SerializeJson();
    }
    /// <summary>
    /// Interfaccia utilizzata per gli oggetti da passare alle chiamate Post che utilizzano un content type = application/x-www-form-urlencoded.
    /// </summary>
    public interface IFormableContent
    {
        /// <summary>
        /// Restituisce le proprietà della classe che si desiderano inviare all'API sotto forma di Dizionario
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetFormValues();
    }
    /// <summary>
    /// Interfaccia utilizzata per gli oggetti restituiti dalle chiamate API. Serve a definire un metodo che permette di popolare la classe a partire da una stringa Json (risposta dell'API)
    /// </summary>
    public interface IApiResult
    {
        /// <summary>
        /// Popola le porprietà della classe a partire da una string Json che contiene le proprietà e i valori
        /// </summary>
        /// <param name="json">String Json con proprietà e valori della classe</param>
        void FillFromJson(string json);
    }
}
