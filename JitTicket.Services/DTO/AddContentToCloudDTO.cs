﻿using JitTicket.Dal.Domain.TicketContent;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.DTO
{
    public class AddContentToCloudListDTO : List<TicketMovieContent>, ISerializableContent
    {
        public string SerializeJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
