﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.DTO
{
    public class BurnTicketToCloudDTO
    {
        public BurnTicketToCloudDTO() { }
        public BurnTicketToCloudDTO(decimal idvend, string nfcuid)
        {
            IdVend = idvend;
            NFCUid = nfcuid;
        }
        public decimal IdVend { get; set; }
        /// <summary>
        /// UID del Tag NFC
        /// </summary>
        public string NFCUid { get; set; }
    }
    public class BurnTicketToCloudListDTO : List<BurnTicketToCloudDTO>, ISerializableContent
    {
        public BurnTicketToCloudListDTO() { }
        public BurnTicketToCloudListDTO(List<(decimal, string)> tickets) : base()
        {
            this.AddRange(tickets.Select(t => new BurnTicketToCloudDTO(t.Item1, t.Item2)));
        }
        public string SerializeJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
