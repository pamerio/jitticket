﻿using JitTicket.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.DTO
{
    /// <summary>
    /// Classe per una generica risposta dalle API
    /// </summary>
    public class BaseResponseDTO
    {
        public bool Succeeded { get; set; }
        public APIErrorCodes ErrorCodes { get; set; }
    }
    public class BaseResponseDTO<T> : BaseResponseDTO
    {
        public T Result { get; set; }
    }
}
