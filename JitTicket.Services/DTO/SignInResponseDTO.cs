﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.DTO
{
    /// <summary>
    /// Oggetto restituito dall'API di SignIn
    /// </summary>
    public class SignInResponseDTO
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string pwd { get; set; }
        public string token { get; set; }
        public string active { get; set; }
        public string code { get; set; }
    }
}
