﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.AuthService
{
    public interface IAuthorizationService
    {
        /// <summary>
        /// Sign-in per l'ottenimento del Token da utilizzarsi per le chiamate successive alle API
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Token</returns>
        Task<(HttpStatusCode responseStatusCode,Guid token)> SignIn(string username, string password);
    }
}
