﻿using JitTicket.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.AuthService
{
    public class AuthorizationService : IAuthorizationService
    {
        public async Task<(HttpStatusCode responseStatusCode, Guid token)> SignIn(string username, string password)
        {
            HttpClient aClient = new HttpClient();

            var signInUri = ConfigurationManager.AppSettings["SIGN_IN_ENDPOINT_ADDRESS"]?.ToString();

            if (signInUri == null)
            {
                throw new Exception("Non è stato trovato l'indirizzo per l'endpoint dell'Api Sign In. Verificare che sia presente nel file di configurazione.");
            }

            var dict = new Dictionary<string, string>();
            dict.Add("op", "signin");
            dict.Add("email", username);
            dict.Add("pwd", password);

            //HttpResponseMessage aResponse = await aClient.GetAsync(new Uri($"{signInUri}?op=signin&email={username}&pwd={password}"));

            HttpResponseMessage aResponse = null;

                var req = new HttpRequestMessage(HttpMethod.Post, signInUri) { Content = new FormUrlEncodedContent(dict) };
                aResponse = await aClient.SendAsync(req);

            if (aResponse.IsSuccessStatusCode)
            {
                var respContent = await aResponse.Content.ReadAsStringAsync();
                SignInResponseDTO response = null;

                try
                {
                   response = JsonConvert.DeserializeObject<SignInResponseDTO>(respContent);
                }
                catch
                {
                    throw new Exception("L'oggetto restituito dall'API di SignIn non è conforme alla classe SignInResponseDTO");
                }
                if (!string.IsNullOrEmpty(response.token))
                {
                    try
                    {
                        var token = new Guid(response.token);
                        if (token == Guid.Empty)
                        {
                            throw new Exception("Si è verificato un problema durante la chiamata a SignIn: il token restituito è nullo");
                        }
                        return (HttpStatusCode.OK, token);
                    }
                    catch(Exception ex)
                    {
                        return (HttpStatusCode.OK, new Guid());  //TODO DA TOGLIERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        var temp = ex;
                        throw new Exception("Errore chiamata SignIn: il token restituito non è del tipo GUID");
                    }
                }
                else
                {
                    throw new Exception("Si è verificato un problema durante la chiamata a SignIn: il token restituito è nullo");
                    //TODO Se all'oggetto SignInResponseDTO aggiungiamo il codice di errore possiamo dare un'informazione in più del tipo di errore che ha generato il token nullo
                }

            }
            else
            {
                return (aResponse.StatusCode, new Guid());
            }
        }
    }
}
