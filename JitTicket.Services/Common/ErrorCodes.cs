﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.Common
{
    /// <summary>
    /// Codici di errore fgenerati dalle API
    /// </summary>
    public enum APIErrorCodes
    {
        None = 0, // Nessun errore
        Error = 1,  // Errore generico non classificato
        BadInputData = 2, // Dati passati all'API non corretti
        TokenNotValid = 3 // Il Token ricevuto non è valido oppure è scaduto
    }
}
