﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.NFCService
{
    public interface INfcService
    {
        /// <summary>
        /// Verifica che l'UID sia valido
        /// </summary>
        /// <param name="uid">UID da verificare</param>
        /// <returns>True se valido, False se non valido</returns>
        Task<bool> CheckUID(string uid);
    }
}
