﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using JitTicket.Dal.Domain.TicketContent;
using JitTicket.Dal.Repositories;
using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using Newtonsoft.Json.Linq;
using Wintic.Data.oracle;

namespace JitTicket.Services.TicketService.TicketContentService
{
    public class TicketContentService : BaseCloudService, ITicketContentService
    {
        private readonly ITicketMovieContentRepository _ticketMovieContentRepository;

        public TicketContentService()
        {
            _ticketMovieContentRepository = new TicketMovieContentRepository();
        }

        #region DB SERVICES

        public async Task<int> CheckNewTickets()
        {
            return await _ticketMovieContentRepository.CheckNewTickets();
        }

        public async Task DeleteNewTicketsOnLocal(IEnumerable<decimal> IdsVendToDelete)
        {
            await _ticketMovieContentRepository.DeleteNewTickets(IdsVendToDelete);
        }

        public async Task<AddContentToCloudListDTO> GetNewTicketsMovieContent()
        {
            var returnObj = new AddContentToCloudListDTO();

            returnObj.AddRange(await _ticketMovieContentRepository.GetNewTicketsMovieContent());

            return returnObj;
        }
        public async Task<IEnumerable<(decimal idVend, string ncfUID)>> GetPassTickets()
        {
            return await _ticketMovieContentRepository.GetPassTickets();
        }
        public async Task DeletePassTicketsOnLocal(IEnumerable<decimal> IdsVendToDelete)
        {
            await _ticketMovieContentRepository.DeletePassTickets(IdsVendToDelete);
        }


        #endregion

        #region CLOUD SERVICES

        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> AddContentToCloud(AddContentToCloudListDTO tickets)
        {
            var addContentUri = ConfigurationManager.AppSettings["ADD_TICKET_CONTENT_ENDPOINT_ADDRESS"]?.ToString();

            if (addContentUri == null)
            {
                throw new Exception("Non è stato trovato l'indirizzo per l'endpoint dell'Api di Aggiunta del contenuto dei biglietti. Verificare che sia presente nel file di configurazione.");
            }

            try
            {
                return await PostAsync<AddContentToCloudListDTO>(addContentUri, tickets);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> BurnContentToCloud(BurnTicketToCloudListDTO tickets)
        {
            var burnContentUri = ConfigurationManager.AppSettings["BURN_TICKET_ENDPOINT_ADDRESS"]?.ToString();

            if (burnContentUri == null)
            {
                throw new Exception("Non è stato trovato l'indirizzo per l'endpoint dell'Api per bruciare dei biglietti. Verificare che sia presente nel file di configurazione.");
            }

            try
            {
                return await PostAsync<BurnTicketToCloudListDTO>(burnContentUri, tickets);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

    }
}
