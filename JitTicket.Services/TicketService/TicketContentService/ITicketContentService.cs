﻿using JitTicket.Dal.Domain.TicketContent;
using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services.TicketService.TicketContentService
{
    public interface ITicketContentService
    {
        #region DB SERVICES

        /// <summary>
        /// Verifica se nella tabella VENDITE_JITNEW sono presenti dei record e quindi nuovi biglietti
        /// </summary>
        /// <returns>
        /// Numero dei record presenti nella tabella VENDITE_JITNEW
        /// </returns>
        Task<int> CheckNewTickets();
        /// <summary>
        /// Restituisce il contenuto di tutti i nuovi biglietti Movie il cui IdVend è pre4sente nella tabella VENDITE_JITNEW
        /// </summary>
        /// <returns></returns>
        Task<AddContentToCloudListDTO> GetNewTicketsMovieContent();
        /// <summary>
        /// Cancella i dati passati dalla tabella VENDITE_JITNEW
        /// </summary>
        /// <returns></returns>
        Task DeleteNewTicketsOnLocal(IEnumerable<decimal> IdsVendToDelete);
        /// <summary>
        /// Verifica se nella tabella VENDITE_JITPASS sono presenti dei record e quindi nuovi biglietti da inviare al cloud
        /// </summary>
        /// <returns>
        /// Numero dei record presenti nella tabella VENDITE_JITPASS
        /// </returns>
        Task<IEnumerable<(decimal idVend, string ncfUID)>> GetPassTickets();
        /// <summary>
        /// Cancella i dati passati dalla tabella VENDITE_JITPASS
        /// </summary>
        /// <returns></returns>
        Task DeletePassTicketsOnLocal(IEnumerable<decimal> IdsVendToDelete);


        #endregion

        #region CLOUD SERVICES

        /// <summary>
        /// Invia il contenuto dei biglietti al Cloud
        /// </summary>
        /// <param name="tickets">Identificati dei biglietti di cui inviare il content al Cloud</param>
        /// <returns>Una tupla contenente eventuali codici di errore restituiti dall'API e lo StatusCode della risposta</returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> AddContentToCloud(AddContentToCloudListDTO tickets);
        // TODO: Gestire il caso in cui ad uno stesso UID possano esserci più ticket collegati
        /// <summary>
        /// Brucia il contenuto di un  biglietto collegato al dato UID. In questo modo il biglietto diventa inutilizzabile
        /// </summary>
        /// <param name="nfcUID">UID del tag NFC</param>
        /// <returns></returns>
        Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> BurnContentToCloud(BurnTicketToCloudListDTO tickets);

        #endregion
    }
}
