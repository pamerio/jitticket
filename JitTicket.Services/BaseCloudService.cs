﻿using JitTicket.Services.AuthService;
using JitTicket.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JitTicket.Services
{
    public class Token
    {
        private static Guid? _token = null;
        private static readonly string PASSWORD = "paolo$$";

        public async static Task<(HttpStatusCode responseStatusCode, Guid token)> GetToken()
        {
            if(_token == null)
            {
                var authService = new AuthorizationService();
                var username = ConfigurationManager.AppSettings["SIGN_IN_USERNAME"]?.ToString();

                if (username == null)
                {
                    throw new Exception("Non è stato trovata la Username per l'API SignIn. Verificare che sia presente nel file di configurazione.");
                }
                try
                {
                    var signResponse = await authService.SignIn(username, PASSWORD);
                    _token = signResponse.token;
                    return signResponse;
                }
                catch
                {
                    throw;
                }
            }
            return (HttpStatusCode.OK, (Guid)_token);
        }
    }
    public class BaseCloudService : IBaseCloudService
    {
        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> GetAsync<TResult>(string endPoint)
        {
            throw new NotImplementedException();
        }

        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostAsync<Tdata>(string endPoint, Tdata data) where Tdata : ISerializableContent
        {
            return await PostAsync(endPoint, data.SerializeJson());
        }
        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostAsync(string endPoint, string jsonContent)
        {
            var tokenResponse = await Token.GetToken();

            if (tokenResponse.responseStatusCode == HttpStatusCode.OK)
            {
                HttpClient aClient = new HttpClient();

                var formValues = new Dictionary<string, string>();
                formValues.Add("token", tokenResponse.token.ToString());
                formValues.Add("data", jsonContent);

                var req = new HttpRequestMessage(HttpMethod.Post, endPoint) { Content = new FormUrlEncodedContent(formValues) };
                var aResponse = await aClient.SendAsync(req);

                if (aResponse.IsSuccessStatusCode)
                {
                    var respContent = await aResponse.Content.ReadAsStringAsync();

                    var response = new BaseResponseDTO
                    {
                        Succeeded = true
                    };

                    return (HttpStatusCode.OK, response);
                }
                return (aResponse.StatusCode, new BaseResponseDTO { Succeeded = false });
            }
            else
            {
                return (tokenResponse.responseStatusCode, new BaseResponseDTO { Succeeded = false });
            }
        }
        public async Task<(HttpStatusCode responseStatusCode, BaseResponseDTO response)> PostFormAsync<Tdata>(string endPoint, Tdata data) where Tdata : IFormableContent
        {
            var token = await Token.GetToken();

            if(token.responseStatusCode == HttpStatusCode.OK)
            {
                HttpClient aClient = new HttpClient();

                var formValues = data.GetFormValues();
                formValues.Add("token", token.token.ToString());

                var req = new HttpRequestMessage(HttpMethod.Post, endPoint) { Content = new FormUrlEncodedContent(formValues) };
                var aResponse = await aClient.SendAsync(req);

                if (aResponse.IsSuccessStatusCode)
                {
                    var respContent = await aResponse.Content.ReadAsStringAsync();

                    var response = new BaseResponseDTO
                    {
                        Succeeded = true
                    };

                    return (HttpStatusCode.OK, response);
                }
                return (aResponse.StatusCode, new BaseResponseDTO { Succeeded = false });
            }
            else
            {
                return (token.responseStatusCode, new BaseResponseDTO { Succeeded = false });
            }
        }
    }
}
