﻿using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JitTicket.WebServiceTest
{
    public static class Utility
    {
        public static string SerializeToJson(this BaseResponseDTO data)
        {
            return JsonConvert.SerializeObject(data);
        }
        public static void WriteResponse(this HttpContext context, bool succeeded, APIErrorCodes? errorCode = null)
        {
            context.Response.Write(new BaseResponseDTO
            {
                Succeeded = succeeded,
                ErrorCodes = errorCode ?? (succeeded ? APIErrorCodes.None : APIErrorCodes.Error)
            }.SerializeToJson());
        }

    }
}