﻿using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JitTicket.WebServiceTest
{
    public abstract class BaseAPI : IHttpHandler    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public virtual void ProcessRequest(HttpContext context)
        {
        }

        protected async Task processRequest(HttpContext context, Func<Task> taskToDo)
        {
            context.Response.ContentType = "text/json";

            string rawData = context.Request["data"];
            string token = context.Request["token"];

            if (string.IsNullOrEmpty(token))
            {
                context.WriteResponse(false, APIErrorCodes.TokenNotValid);
            }

            if (!string.IsNullOrEmpty(rawData))
            {
                try
                {
                    var tickets = JsonConvert.DeserializeObject<AddContentToCloudListDTO>(rawData);

                    await taskToDo();

                    context.WriteResponse(true);

                }
                catch (Exception ex)
                {
                    //TODO Gestione errore

                    context.WriteResponse(false, APIErrorCodes.BadInputData);
                }

            }
            else
            {
                context.WriteResponse(false, APIErrorCodes.BadInputData);
            }

        }
    }
}