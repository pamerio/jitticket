﻿using JitTicket.Services.Common;
using JitTicket.Services.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JitTicket.WebServiceTest
{
    /// <summary>
    /// Aggiunta contenuto biglietti
    /// </summary>
    public class AddTicketContent : BaseAPI, IHttpHandler
    {
        public override async void ProcessRequest(HttpContext context)
        {
            await processRequest(context, () => {
                return Task.Run(() => {
                    // TODO Fare qualcosa
                });
            });
        }
    }
    /// <summary>
    /// Bruci i biglietti
    /// </summary>
    public class BurnTicket : BaseAPI, IHttpHandler
    {
        public override async void ProcessRequest(HttpContext context)
        {
            await processRequest(context, () => {
                return Task.Run(() => {
                    // TODO Fare qualcosa
                });
            });
        }
    }
}